public class TreeNode {
    static final String DELIMETER="│";
    static final String CORNERUP="┌";
    static final String CORNERDOWN = "└";
    static final String BRANCK ="┤";
    private int data;
    private TreeNode leftChild;
    private TreeNode rightChild;

    public TreeNode(int data) {
        this.data = data;
    }

    public TreeNode find(int data){
        if(this.data == data) {
            return this;}
        if((data < this.data) && (leftChild != null)) {
            return leftChild.find(data);
        }
        if((data > this.data) && (rightChild != null)) {
            return rightChild.find(data);
        }
        return null;
    }

    public void add(int data) {
        if(data >= this.data){
            if(this.rightChild == null){
                this.rightChild = new TreeNode(data);
            }
            else {
                this.rightChild.add(data);
            }
        } else{
            if(this.leftChild == null){
                this.leftChild = new TreeNode(data);
            }
            else {
                this.leftChild.add(data);
            }

        }

    }

    public String prettyPrint(){

        String res = "";
        int i = 0;
        res = res + "   " +CORNERUP  + "\n";
        while( i < this.countLeft()-1) {
            res = res + "   " +DELIMETER + "\n";
            i = i+1;
        }
        res = res + new TreeNode(data).getData() +  BRANCK + "\n";
        i = 0;
        while( i < this.countRight()-1) {
            res = res + "   " +DELIMETER + "\n";
            i = i+1;
        }
        res = res + "   " +CORNERDOWN + "\n";

        //System.out.println(res);
        return res;
    }
    public String prettyPrint(TreeNode boo5){
        String boo = boo5.prettyPrint();

        String res = "";
        int i = 0;
        res = res + "   " +CORNERUP + boo + "\n";
        while( i < this.countLeft()-1) {
            res = res + "   " +DELIMETER + "\n";
            i = i+1;
        }
        res = res + new TreeNode(data).getData() +  BRANCK + "\n";
        i = 0;
        while( i < this.countRight()-1) {
            res = res + "   " +DELIMETER + "\n";
            i = i+1;
        }
        res = res + "   " +CORNERDOWN + "\n";

        //System.out.println(res);
        return res;
    }
    public int countLeft(){
        int i = 0;
        TreeNode meow = new TreeNode(data);
        while( meow.getLeftChild() != null){

            i = i + 1;
            meow = meow.find(meow.getLeftChild().getData());

        }
        return i;
    }

    public int countRight(){
        int i = 0;
        TreeNode meow = new TreeNode(data);
        while( meow.getRightChild() != null){

            i = i + 1;
            meow = meow.find(meow.getRightChild().getData());

        }
        return i;
    }

    public TreeNode getLeftChild() {
        return leftChild;}


    public void setLeftChild(TreeNode leftChild) {
        this.leftChild = leftChild;
    }

    public TreeNode getRightChild() {
        return rightChild;
    }

    public void setRightChild(TreeNode rightChild) {
        this.rightChild = rightChild;
    }

    public int getData() {
        return data;
    }
}
