public class BinaryTree implements PrintableTree{
    static final String DELIMETER="│";
    static final String CORNERUP="┌";
    static final String CORNERDOWN = "└";
    static final String BRANCK ="┤";
    static final String SPACE =" ";
    private TreeNode root;

    @Override
    public void add(int i) {
        if(root == null){
            root = new TreeNode(i);
        } else {
            root.add(i);
        }

    }

    public String prettyPrintT3st(){
        TreeNode meow = root;
        int i = 0;
        String res = "";
        while( meow.getLeftChild() != null && meow.getRightChild()!= null){

            i = i + 1;
            meow = meow.find(meow.getLeftChild().getData());
            res = res + root.prettyPrint(meow);

        }

        //String up = root.getRightChild().prettyPrint();
       // String res = up + root.prettyPrint();
        return res;

    }
    @Override
    public String prettyPrint() {

        int a = root.getData();
        TreeNode bla = root.getLeftChild();
        int b = root.getLeftChild().getData();
        int c = bla.getLeftChild().getData();
        int i = 0;
        String res = "";

        res = res + "   " +CORNERUP + "\n";
        while( i < this.countLeft()-1) {
             res = res + "   " +DELIMETER + "\n";
             i = i+1;
        }
        res = res + a +  BRANCK + "\n";
        i = 0;
        while( i < this.countRight()-1) {
            res = res + "   " +DELIMETER + "\n";
            i = i+1;
        }
        res = res + "   " +CORNERDOWN + "\n";


        return res;
    }

    public int countLeft(){

        int i = 0;

        /*if (meow != null){
            if(meow.getLeftChild() != null){
                i = i +1;
            }
        }
        i = i + meow.getLeftChild().countLeft();*/
        TreeNode meow = root;
        while( meow.getLeftChild() != null){

            i = i + 1;
            meow = meow.find(meow.getLeftChild().getData());

        }
        return i;
    }

    public int countRight(){

        int i = 0;

        /*if (meow != null){
            if(meow.getLeftChild() != null){
                i = i +1;
            }
        }
        i = i + meow.getLeftChild().countLeft();*/
        TreeNode meow = root;
        while( meow.getRightChild() != null){

            i = i + 1;
            meow = meow.find(meow.getRightChild().getData());

        }
        return i;
    }

    public TreeNode find(int data){
        if(root != null){
            return root.find(data);
        }
        return null;
    }

    public TreeNode getRoot() {
        return root;
    }
}
