public interface PrintableTree {

    void add(int i);
    String prettyPrint();

    static PrintableTree getInstance() {
        BinaryTree tree = new BinaryTree();
        tree.add(123);
        tree.add(11);
        tree.add(200);
        tree.add(1);
        tree.add(100);
        tree.add(150);
        tree.add(2000);

        System.out.println(tree.countLeft());

        return null;
    }
}
